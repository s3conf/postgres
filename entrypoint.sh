#!/usr/bin/env bash

set -e
export $(s3conf env -m)
. "docker-entrypoint.sh"
