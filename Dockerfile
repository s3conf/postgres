FROM postgres:9.6.3

COPY ./entrypoint.sh /app/entrypoint.sh

RUN apt-get update \
    && apt-get -y --no-install-recommends install python3 python3-setuptools python3-pip git-core \
    && pip3 install -U s3conf==0.6.0 \
    && apt-get autoremove -yqq \
    && apt-get clean \
    && rm -Rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
    && chmod 755 /app/entrypoint.sh 

EXPOSE 5432
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["postgres"]
